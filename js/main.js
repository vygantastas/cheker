$(document).ready(function() {

		board = [
			[0, 1, 1, 1],
			[0, 0, 0, 1],
			[1, 0, 0, 1],
			[0, 2, 1, 0],
			[0, 0, -1, -1], //4
			[-1, -1, -1, 0],
			[-1, -1, -1, -1],
			[-1, -1, -1, -1]
		];

		b = [
			[5, 0, 5, 1, 5, 1, 5, 1],
			[0, 5, -2, 5, 0, 5, 1, 5],
			[5, 0, 5, 0, 5, 0, 5, 1],
			[0, 5, 0, 5, 1, 5, 1, 5],
			[5, 0, 5, 0, 5, 0, 5, -1], //4
			[-1, 5, -1, 5, 0, 5, 0, 5],
			[5, -1, 5, -1, 5, -1, 5, -1],
			[-1, 5, -1, 5, -1, 5, -1, 5]
		];
		di = [
			[-1, -1, -2, -2], //1
			[-1, 1, -2, 2], //2
			[1, 1, 2, 2], //3
			[1, -1, 2, -2] //4
		];
		dir = [
			[0, -1, -1, -2], //1
			[1, 1, 1, 2], //3
			[1, -1, 1, -2], //2
			[0, 1, -1, 2], //4
			[-1, -1, -1, -2], //v k 1
			[0, 1, 1, 2], //z d		3
			[0, -1, 1, -2], //v d	2
			[-1, 1, -1, 2] //z k	4
		]; // nukirtus leidzia kirsti ir kitai savai saskei
		var cheker = -1,
			patch = [
				[0, 0, 0]
			];
		selected = false;

		var cutted = [];

		ind = 0;
		player = -1;
		c = -1;
		sel_e = sel_s = cut_e = cut_s = -1;

		oneCuted = false;
		cl = 0; //first_cl = 0;
		function board_gener() {
			var board_html = '';
			for (var i = 0; i < 8; i++)
				for (j = 0; j < 8; j++)
					if ((i + j) % 2 == 1) {
						chess = '<div class="cheker ';
						console.log(b[i][j]); //[(j - (j % 2)) / 2]);
						switch (b[i][j]) { //(j - (j % 2)) / 2]) {
							case -1:
								chess += 'white';
							case 1:
								chess += '"></div>';
								break;
							case -2:
								chess += 'white';
							case 2:
								chess += '"><div class ="queen"></div></div>';
								break;
							default:
								chess = '';
						}
						board_html += '<div class="block" style="top:' + i * 100 + 'px; left:' + j * 100 + 'px;">' + chess + '</div>';
					}
			$('.lenta').html(board_html);
			if (player == 1) $('.field').css('background-color', '#000');
			else $('.field').css('background-color', '#ddd'); //			return cl;
			return;
		}

		$('.lenta').on('click', '.block', function() {
			cl = $(this);
			s = parseInt($(this).css('left'));
			e = parseInt($(this).css('top'));
			play(e / 100, (s - (s % 100)) / 100);
		});
		board_gener();
		


		function play(e, s) {
			console.log(b[e][s], 'selected', e, s);
			//			testBoardPlace(e, s, [5, -5]);
			console.log(wOrB(b[e][s]), 'what selected player', player);

			if (wOrB(b[e][s]) == player && (canSel(e, s))) {

				console.log('select 111111');
				select(e, s);
			} else if ((wOrB(b[e][s]) == -player) && oneCuted) {
				changePlayer();
			} else if ((b[e][s] == 0) && selected) {
				if (!move(e, s)) {}
			}
			console.log("play2", player, e, s, oneCuted, '-cutted, selected -', selected, sel_e, sel_s);
		}

		function canCut(e, s, cut) {
			if (b[e, s] == 2 || b[e, s] == -2)
				queen = true;
			cheker = b[e][s];
			//			pushToPath (e, s, cheker);
			for (var i = 0; i < 4; i++) {
				if (canCut(e, s, player, cheker, i, sel_e, sel_s, queen, cut)) {
					if (cut) {
						cut_e = e;
						cut_s = s;
					}
					return true;
				}

			}
			return false;

		}

		function testDir(e, s, player, cheker, direction, now_e, now_s, queen = false, cut = false) {
			patch[0] = [0, 0, 0];
			var ind = 1,
				t_e = 0,
				t_s = 0;
			d_e = di[direction][0];
			d_s = di[direction][1];
			//			queen = false;
			//t_e = e + di [direction][0];
			//t_s = s + di [direction][1];
			//			if (queen)
			t_e += d_e;
			t_s += d_s;
			while (queen && testBoardPlace(t_e, t_s, ind, 0)) {
				pushToPath(t_e, t_s, 0);
				ind++;
				t_e += d_e;
				t_s += d_s;
			}
			if (!(testBoardPlace(t_e, t_s, [-player, -2 * player])))
				return false;
			else {
				pushToPath(t_e, t_s, 0);
				ind++;
				t_e += d_e;
				t_s += d_s;
				//				return false;
			}
			if (!(testBoardPlace(t_e, t_s, ind, 0)))
				return false;
			while (!(t_e == now_e && t_s == now_s)) {
				pushToPath(t_e, t_s, 0);
				ind++;
				t_e += d_e;
				t_s += d_s;
			}
			do {
				pushToPath(t_e, t_s, 0);
				ind++;
				t_e += d_e;
				t_s += d_s;
			} while (testBoardPlace(t_e, t_s, ind, 0));
			if (t_e == now_e && t_s == now_s) {
				pushToPath(t_e, t_s, cheker);
				return true;
			} else
				return false;
		}



		//else {
		//	for (var i = 0; i < find.length; i++) {
		//		t_e = e + di[direction][0];
		//		t_s = s + di[direction][1];
		//		if (coordInBoard(t_e, t_s) && wOrB(b[t_e][t_s]) == find[i])
		//			if (cut) {
		//				path[i][0] = t_e;
		//				path[i][1] = t_s;
		//				path[i][2] = 0;
		//
		//			}
		//		else
		//			return false;
		//	}
		//	if (cut) {
		//		path[0][0] = e;
		//		path[0][1] = s;
		//		//				path[0][2] = 0;
		//	}
		//	return true;
		//}


		function testBoardPlace(e, s, symb) {
			return (coordInBoard(e, s) && ((b[e][s]) == symb[0]) || (b[e][s] == symb[1]));
		}

		function pushToPath(d_e, d_s, ind, symb) {
			//t_e = t_e + di[direction][0];
			//t_s = t_s + di[direction][1];
			path[ind][0] = t_e;
			path[ind][1] = t_s;
			path[ind][2] = symb;
		}

		function changePlayer() {
			console.log('changePlayer');
			player = -player;
			selected = false;
			oneCuted = false;
			changeTimer();
		}

		function move(e, s) {
			console.log('move');

			if (canCutAround(sel_e, sel_s, e, s) || canMove(sel_e, sel_s, e, s)) {
				console.log('moooooooooooooggggggggggggggggggoooove', e, s);
				b[sel_e][sel_s] = 0;
				b[e][s] = cheker;
				cut_e = e;
				cut_s = s;
				board_gener();
				//if (oneCuted) {
				//	return canSel(e, s);
				//}
				return false;
			}
		}

		
		// weight: 1 - negali eit 2 - eina 4 - kerta 8 - kapoja
		function select(e, s) {
			console.log('SELECT FUNKCIJA');
			if (canCutAround(e, s) && (e == cut_e) && (s == cut_s)) {
				weight = 8;
			} else if (canCutAround(e, s)) {
				weight = 4;
			} else if (canMove(e, s)) {
				weight = 2;
			} else {
				weight = 1;
			}
			
			if (weight == 8) {
				selected = true;
			} else {
				if (someCanCut(player)){
					weightOther = 4;
				} else {
					weightOther = 2;
				}
			}
			if (weight >= weightOther){
				selected = true;

			} else {
				selected = false;
			}
			if (selected) {
			cheker = b[e][s];
			sel_e = e;
			sel_s = s;
			$('*').removeClass('select');
			cl.addClass('select');
			}
		}

		function canSel(e, s) {
			console.log('canSel', oneCuted, e, s);
			if (selected && (sel_e == e) && (sel_s == s)) {
				selected = false;
				$('*').removeClass('select');
				return false;
			}
			if (!(oneCuted)) {
				if (canCutAround(e, s) || (!someCanCut(player) && canMove(e, s))) {
					console.log("can cut or move");
					return true;
				}
			} else if (canCutAround(e, s) && (e == cut_e) && (s == cut_s)) {
				console.log("can only cut");
				return true;
			}
			console.log("NOT canSel");
			return false;
		}

		function canMove(e, s, now_e = -1, now_s = -1) {
			var l = player > 0 ? 2 : 0;
			console.log('can can_move2');

			for (var i = 0; i < 2; i++) {
				e1 = e + di[l + i][0];
				s1 = s + di[l + i][1];
				if (coordInBoard(e1, s1) && (b[e1][s1]) == 0) {
					if (oneCuted) {
						console.log("mmmove oneCuted");
						return false;
					}
					if (!(selected)) {
						console.log("");
						return true;
					} else if ((e1 == now_e) && (s1 == now_s)) {
						return true;
					}
				}
			}
			return false;
		}

		function canCutAround(e, s, now_e = -1, now_s = -1) {
			console.log('canCutAround', e, s, now_e, now_s);
			for (var i = 0; i < 4; i++) {
				e2 = e + di[i][2];
				s2 = s + di[i][3];
				e1 = e + di[i][0];
				s1 = s + di[i][1];
				console.log(e2, s2);
				if (coordInBoard(e2, s2) && b[e2][s2] == 0 && wOrB(b[e1][s1]) == -player) {
					console.log('i  ', i, e2, s2, b[e2][s2], 'player', player, 'selected', selected);
					if (!(selected)) {
						console.log("selected====", selected);
						return true;
					} else if ((e2 == now_e) && (s2 == now_s)) {
						console.log(e2, now_e, 'arounddddddddddddddddd', s2, now_s);
						b[e1][s1] = 0;
						console.log('aroundddddddddddddddddccccccccccccccccccccdddddddddddddd');
						oneCuted = true;
						selected = false;
						//sel_e = e2;
						//sel_s = s2;
						return true;
					}
				}
				console.log("ret = truuuuuuuuuuue ");
			}
			return false;
		}

		function someCanCut(color) {
			for (var i = 0; i < 8; i++)
				for (var j = 0; j < 8; j++) {
					if (b[i][j] == color && canCutAround(i, j)) {
						//[(i+2*j)%2+1])
						console.log("some can cuttttttttttt");
						return true;
					}
				}
			return false;
		}

		function oneDirFirst() {}

		function oneDirSecond() {

			//		coordInBoard
		}

		function coordInBoard(e, s) {
			//			console.log(e >= 0 && e < 8 && s >= 0 && s < 8);

			return (e >= 0 && e < 8 && s >= 0 && s < 8);
		}

		function wOrB(n) {
			var ret = 0;
			if (n > 0) ret = 1;
			else if (n < 0) ret = -1;
			return ret;
		}
		
		//TIMER
		var timeW = 0; //turn timer starts at 0
		var timeB = 0;
		//Timer function
		setInterval(timer, 1000);
		function timer() {
			if (player == -1) {
				timeW++;
			} else {
				timeB++;
			}
			$('#black').html(timerHtml(timeB));
			$('#white').html(timerHtml(timeW));
		}
		//Changes side size
		function changeTimer() {
			colH = timeB + timeW;
			colB = (timeB * 100) / colH;
			if (colB > 90) {
				colB = 90;
			} else if (colB < 10) {
				colB = 10;
			}
			$('.side-black').css('height', colB + '%');
		}
		//Adds timer values
		function timerHtml(time) {
			seconds = time % 60;
			minutes = ((time - seconds) / 60 % 60).toFixed(0);
			hours = ((time - time % 3600) / 3600).toFixed(0);
			return hours + ' : ' + minutes + ' : ' + seconds;
		}
	}

);